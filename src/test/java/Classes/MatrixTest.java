package Classes;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MatrixTest {
    @org.testng.annotations.Test
    public void testGetElement() {

    }

    @org.testng.annotations.Test
    public void testTestSetElement() {
    }

    @org.testng.annotations.Test
    public void testTestDet() throws IllegalAccessException {
        Matrix factorial = new Matrix(5);
        for (int i = 0; i < 5; i++){
            factorial.setElement(i,i,(double) i+1);
        }
        Assert.assertEquals(factorial.det(), 120);
    }
}