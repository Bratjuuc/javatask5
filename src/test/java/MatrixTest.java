import Classes.DiagMatrix;
import Classes.Matrix;
import Classes.MatrixService;
import Classes.UpTriangleMatrix;
import Interfaces.IMatrix;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixTest {

    Matrix zero = new Matrix(new Double[][]{{0.0, 0.0}, {0.0, 0.0}});
    Matrix std = new Matrix(new Double[][]{{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}});
    DiagMatrix diag = new DiagMatrix(new Double[] {1.0, 2.0, 3.0, 4.0, 5.0});
    UpTriangleMatrix triangle = new UpTriangleMatrix(new Double[][]{{1.0, 2.0, 3.0}, {0.0, 4.0, 5.0}, {0.0, 0.0, 6.0}});


    // --- sizeTest --- ///

    @Test
    public void sizeStd() {
        assertEquals(3, std.getDimension());
    }

    @Test
    public void sizeDiag() {
        assertEquals(5, diag.getDimension());
    }

    @Test
    public void sizeTriangle() {
        assertEquals(3, triangle.getDimension());
    }


    // --- getElemTest --- //

    @Test
    public void getElemStd() {
        assertEquals(3, std.getElement(0, 2), 1E-9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getElemStdFail() {
        std.getElement(0, 3);
    }


    @Test
    public void getElemDiagOn() {
        assertEquals(3, diag.getElement(2, 2), 1E-9);
    }

    @Test
    public void getElemDiagOut() {
        assertEquals(0, diag.getElement(2, 1), 1E-9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getElemDiagFail() {
        diag.getElement(0, 5);
    }


    @Test
    public void getElemTriangleOn() {
        assertEquals(3, triangle.getElement(0, 2), 1E-9);
    }

    @Test
    public void getElemTriangleOut() {
        assertEquals(0, triangle.getElement(2, 1), 1E-9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getElemTriangleFail() {
        triangle.getElement(0, 3);
    }


    // --- setElemTest --- //

    @Test
    public void setElemStd() {
        std.setElement(1, 2, 666.0);
        assertEquals(666, std.getElement(1, 2), 1E-9);
        std.setElement(1, 2, 6.0);
    }

    @Test
    public void setElemDiag() {
        diag.setElement(4, 4, 666.0);
        assertEquals(666, diag.getElement(4, 4), 1E-9);
        diag.setElement(4, 4, 5.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setElemDiagFail() {
        diag.setElement(2, 0, 666.0);
    }

    @Test
    public void setElemTriangle() {
        triangle.setElement(0, 2, 666.0);
        assertEquals(666, triangle.getElement(0, 2), 1E-9);
        triangle.setElement(0, 2, 3.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setElemTriangleFail() {
        triangle.setElement(1, 0, 666.0);
    }


    // --- determinantTest --- //

    @Test
    public void determinantStd() throws IllegalAccessException {
        assertEquals(0, std.det(), 1E-9);
        assertEquals(0, zero.det(), 1E-9);
    }

    @Test
    public void determinantDiag() throws IllegalAccessException {
        assertEquals(120, diag.det(), 1E-9);
    }

    @Test
    public void determinantTriangle() throws IllegalAccessException {
        assertEquals(24, triangle.det(), 1E-9);
    }


    // --- MatrixServiceTest --- //

    @Test
    public void arrangeMatricesTest() {
        IMatrix[] sortedMtx = new IMatrix[]{zero, std, diag, triangle};
        MatrixService.arrangeMatrices(sortedMtx);
        assertArrayEquals(new IMatrix[]{ zero, std, triangle, diag}, sortedMtx);
    }
}