package Interfaces;

public interface IMatrix {
    public Double getElement(int i, int j) throws IllegalAccessError;
    public void setElement(int i, int j, Double val) throws IllegalAccessError;
    public Double det();
}
