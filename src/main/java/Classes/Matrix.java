package Classes;

import Interfaces.IMatrix;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Comparator;

public class Matrix implements IMatrix, Comparable<Matrix> {
    private final Double[] elements;
    private final int dimension;
    protected Double det = 0.0;
    protected Boolean detFlag = false;


    public Matrix(int n) throws IllegalArgumentException{
        if (n <= 0){
            throw new IllegalArgumentException("Размер матрицы должен быть положительным");
        }
        dimension = n;
        elements = new Double[n*n];
        for (int i = 0; i < getDimension(); i++){
            for (int j = 0; j < getDimension(); j++) {
                elements[i*dimension + j] = 0.0;
            }
        }
    }
    public Matrix(Double[][] array) throws IllegalArgumentException{
        int l1 = array.length;
        if (l1 == 0){
            throw new IllegalArgumentException("Матрица должна быть не пустой");
        }

        int l2 =  array[0].length;
        if (l2 == 0 || l1 != l2){
            throw new IllegalArgumentException("Матрица должна быть квадаратной");
        }

        dimension = array.length;
        elements = new Double[dimension*dimension];
        for (int i = 0; i < l1; i++){
            for (int j = 0; j < l1; j++){
                setElement(i, j, array[i][j]);
            }
        }

    }


    public Matrix(Matrix copy){
        dimension = copy.dimension;
        elements = new Double[copy.elements.length];

        for (int i = 0; i < copy.elements.length; i++){
            elements[i] = copy.elements[i];
        }


        det = copy.det;
        detFlag = copy.detFlag;
    }

    public Double getElement(int i, int j) throws IllegalArgumentException {
        if (i >= dimension || j >= dimension || i < 0 || j < 0){
            throw new IllegalArgumentException();
        }
        return elements[i*dimension+j];
    }

    public void setElement(int i, int j, Double val) throws IllegalArgumentException {
        if ( i >= dimension || j >= dimension || i < 0 || j < 0){
            throw new IllegalArgumentException();
        }
        if (getElement(i, j) != val){
            detFlag = false;
        }
        elements[i*dimension+j] = val;
    }

    public int getDimension() {
        return dimension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix = (Matrix) o;
        return Arrays.equals(elements, matrix.elements);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(elements);
    }

    public Double det() throws IllegalArgumentException {
        if (detFlag){
            return det;
        }

        Matrix temp = new Matrix(this);

        for (int i = 0; i < temp.dimension; i++){
            if(temp.searchfor(i)){
                return (double) 0;
            }
            for (int j=i; j < temp.dimension; j++){
                temp.substract(i, j);
            }
        }
        Double result = 1.0;
        for (int i = 0; i < dimension; i++){
            result *= temp.getElement(i,i);
        }
        detFlag = true;
        det = result;
        return result;
    }

    private Boolean searchfor(int i) throws IllegalArgumentException {
        //Ищет строку с ненулевым итым элементом и ставит его на итое место со сменой знака
        if (i < 0 || i >= dimension*dimension){
            throw new IllegalArgumentException();
        }
        for (int j = i; j < dimension; j++){
            if (Math.abs(getElement(j,i)) > 1e-7){
                if (i != j){
                    Double temp;
                    for (int k = 0; k < dimension; k++){
                        temp = getElement(i,k);
                        setElement(i,k, getElement(j,k));
                        setElement(j,k,-temp);
                    }
                }
                return false;
            }
        }
        return true;
    }

    private void substract(int i, int j) throws IllegalArgumentException {
        if (i < 0 || i >= dimension*dimension){
            throw new IllegalArgumentException();
        }
        if (j < 0 || j >= dimension*dimension){
            throw new IllegalArgumentException();
        }
        if (i > j){
            throw new IllegalArgumentException();
        }
        for (int k = i; k < dimension; k++){
            setElement(j,k, getElement(j,k)-getElement(i,k)*getElement(j,i)/getElement(i,i));
        }
    }

    @Override
    public int compareTo(@NotNull Matrix o) {
        return det().compareTo(o.det());
    }
}
