package Classes;

public class UpTriangleMatrix extends Matrix {
    public UpTriangleMatrix(int n) throws IllegalArgumentException {
        super(n);
    }
    public UpTriangleMatrix(Double [][] n) throws IllegalArgumentException {
        super(n.length);
        for (int i = 0; i < getDimension(); i++){
            for (int j = i; j < getDimension(); j++) {
                setElement(i, j, n[i][j]);
            }
        }
        //super(n);
    }

    @Override
    public void setElement(int i, int j, Double val) throws IllegalArgumentException {
        if (i > j && Math.abs(val) > 1e-9){
            throw new IllegalArgumentException("Попытка испортить верхнетреугольность матрицы");
        }
        super.setElement(i, j, val);
    }

    @Override
    public Double det() {
        if (detFlag){
            return det;
        }
        Double result = (double)1;
        for (int i = 0; i < getDimension(); i++){
            result *= getElement(i,i);
        }
        detFlag = true;
        return result;
    }
}
