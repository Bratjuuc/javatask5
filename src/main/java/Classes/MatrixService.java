package Classes;

import Interfaces.IMatrix;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

public class MatrixService {
    public static void arrangeMatrices(IMatrix[] array){
        Arrays.sort(array, Comparator.comparing(IMatrix::det));
    }
}
