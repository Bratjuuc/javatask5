package Classes;

public class DiagMatrix extends Matrix {
    public DiagMatrix(int n) throws IllegalArgumentException {
        super(n);
    }

    public DiagMatrix(Double[] array){
        super(array.length);
        if (array.length == 0){
            throw new IllegalArgumentException("Попытка инициализации диагональной матрицы пустым массивом");
        }
        for (int i = 0; i < array.length; i++){
            setElement(i,i,array[i]);
        }
    }

    public Double getElement(int i) throws IllegalArgumentException {
        if (i < 0 || i >= getDimension()){
            throw new IllegalArgumentException();
        }
        return super.getElement(i,i);
    }

    @Override
    public void setElement(int i, int j, Double val) throws IllegalArgumentException {
        if (i != j && Math.abs(val) > 1e-6){
            throw new IllegalArgumentException();
        }
        super.setElement(i, j, val);
    }

    @Override
    public Double getElement(int i, int j) throws IllegalArgumentException {
        if ( i >= getDimension() || j >= getDimension() || i < 0 || j < 0){
            throw new IllegalArgumentException();
        }
        if (i != j){
            return 0.0;
        }
        else {
            return super.getElement(i,j);
        }
    }

    public void setElement(int i, Double val) throws IllegalArgumentException {
        setElement(i, i, val);
    }

    @Override
    public Double det() throws IllegalArgumentException {
        if (detFlag){
            return det;
        }
        Double result = (double)1;
        for (int i = 0; i < getDimension(); i++){
            result *= getElement(i);
        }
        detFlag = true;
        det = result;
        return result;
    }
}
