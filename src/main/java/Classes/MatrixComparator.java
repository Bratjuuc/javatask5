package Classes;

import Interfaces.IMatrix;

import java.util.Comparator;

public class MatrixComparator implements Comparator<IMatrix> {
    private boolean reverse = false;

    @Override
    public int compare(IMatrix o1, IMatrix o2) {
        int result = 0;
        result = o1.det().compareTo(o2.det());
        return reverse ? result : -result;
    }

    @Override
    public Comparator<IMatrix> reversed() {
        reverse = !reverse;
        return this;
    }

    @Override
    public Comparator<IMatrix> thenComparing(Comparator<? super IMatrix> other) {
        return this;
    }

    public MatrixComparator(){

    }
}
